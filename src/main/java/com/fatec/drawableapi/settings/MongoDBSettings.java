package com.fatec.drawableapi.settings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import com.mongodb.client.MongoDatabase;

import org.springframework.stereotype.Component;

/**
 * 
 * Classe para recuperar informações sobre o mongo inseridas no arquivo aplication.properties 
 * localizado em src/main/resources/
 *
 */

@Component
public class MongoDBSettings {

	private final MongoDbFactory mongo;

	@Autowired
	public MongoDBSettings(MongoDbFactory mongo) {
		this.mongo = mongo;
	}

	public MongoDatabase getMongoConnection() {
		return mongo.getDb();
	}
}
