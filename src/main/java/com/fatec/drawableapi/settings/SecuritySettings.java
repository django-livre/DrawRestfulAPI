package com.fatec.drawableapi.settings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.fatec.drawableapi.model.UserDetailMapping;

@Configuration
public class SecuritySettings extends WebSecurityConfigurerAdapter {

	/**
	 * 
	 * Cria instância da classe que realiza o ORM dos usuários
	 * @return UserDetailsService
	 */
	
	@Bean
	public UserDetailsService mongoUserDetails() {
		return new UserDetailMapping();
	}

	/**
	 * Autentica e valida usuário
	 * 
	 */
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		UserDetailsService userDetailsService = mongoUserDetails();
		auth.userDetailsService(userDetailsService);

	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		/** Desabilitando CSRF 
		* 	Este link contém mais informações sobre:
		*	http://blog.caelum.com.br/protegendo-sua-aplicacao-web-contra-cross-site-request-forgerycsrf/
		**/ 
		http
			.csrf()
			.disable();
		
		/** Habilita o Basic Authentication no Spring security com o metodo HttpBasic
		 * 
		 * O primeiro escopo define que toda autenticação deve ser autenticada antes de ser realizada
		 * O segundo escopo (os escopos são separados por and) define que a aplicação será stateless 
		 * (a cada requisição o usuário e a senha será enviado no cabeçalho)
		 * Nos ultimos escopos as URLs no método antMatchers são liberadas para acesso público (sem autenticação)
		 * */
		
		http
			.authorizeRequests()
			.anyRequest()
			.authenticated()
		.and()
			.httpBasic()
		.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
			.authorizeRequests()
			.antMatchers(HttpMethod.GET, "/","/docs", 
					"/configuration/**", "/swagger-resources/**",
					"/swagger-ui.html", "/webjars/**")
			.anonymous()
			.anyRequest()
			.permitAll()
		.and()
			.authorizeRequests()
			.antMatchers(HttpMethod.POST, "/login**")
			.anonymous()
			.anyRequest()
			.permitAll()
		.and()
			.authorizeRequests()
			.antMatchers(HttpMethod.PUT, "/register**")
			.anonymous()
			.anyRequest()
			.permitAll();
	}

	/**
	 * 
	 * Define o enconder para que o password seja gravado com um hash gerado
	 * @return PasswordEncoder
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
