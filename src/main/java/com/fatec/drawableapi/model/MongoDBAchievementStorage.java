package com.fatec.drawableapi.model;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.fatec.drawableapi.api.AchievementStorage;
import com.fatec.drawableapi.api.MongoUserRepository;

import net.sf.esfinge.gamification.achievement.Achievement;
import net.sf.esfinge.gamification.achievement.Point;

/**
 * 
 * Implementação da interface de storage de pontos e rankings utilizada pelo esfinge gamification
 * 
 */
@Component
public class MongoDBAchievementStorage implements AchievementStorage {

	@Autowired
	private MongoUserRepository mongoRepository;

	@Override
	public void insert(Object user, Achievement a) {
		Logger.getLogger(this.getClass().getName(), "Metodo insert ainda não implementado!!!");
	}

	@Override
	public void delete(Object user, Achievement a) {
		Logger.getLogger(this.getClass().getName(), "Metodo delete ainda não implementado!!!");
	}

	@Override
	public void update(Object user, Achievement a) {

		Point point = (Point) a;
		String id = user.toString();
		Optional<User> optionalUser = mongoRepository.findById(id);

		if (optionalUser.isPresent()) {
			Integer quantity = point.getQuantity();
			User u = optionalUser.get();

			u.setPoints(quantity);
			mongoRepository.save(u);

		} else {
			Logger.getLogger(this.getClass().getName(), "Objeto " + user + " não existe!!!");
		}

	}

	@Override
	public Achievement select(Object user, String achievName) {

		String id = user.toString();
		Point p;
		Optional<User> optionalUser = mongoRepository.findById(id);

		if (optionalUser.isPresent()) {

			User u = optionalUser.get();
			Integer nowPoints = (Integer) u.getPoints();

			p = new Point(nowPoints, achievName);

		} else {
			p = null;
		}

		return p;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Achievement> select(Object user) {

		String id = user.toString();
		Optional<User> optionalUser = mongoRepository.findUserAndAchievementById(id);
		Map<String, Achievement> map;

		if (optionalUser.isPresent()) {
			User u = optionalUser.get();
			map = u.toMongoDocument().toMap();

		} else {
			Logger.getLogger(this.getClass().getName(), "Erro ao converter achievements em Map ");
			map = null;
		}

		return map;
	}

	@Override
	public List<User> makeRanking() {
		List<User> users = mongoRepository.findAll(new Sort(Sort.Direction.DESC, "points"));
		return users;
	}

}
