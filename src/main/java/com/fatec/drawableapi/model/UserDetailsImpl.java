package com.fatec.drawableapi.model;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 
 * Classe utilizada para ORM pelo Spring
 * 
 */

public class UserDetailsImpl implements UserDetails {

	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String email;
	private String password;
	private int points;
	private List<GrantedAuthority> grantedAuthorities;

	public UserDetailsImpl(User user) {
		this.id = user.getId();
		this.setName(user.getName());
		this.setEmail(user.getEmail());
		this.password = user.getPassword();
		this.setPoints(user.getPoints());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return id;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}
}
