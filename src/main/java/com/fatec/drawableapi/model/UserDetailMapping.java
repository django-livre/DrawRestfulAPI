package com.fatec.drawableapi.model;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.fatec.drawableapi.api.MongoUserRepository;

@Component
public class UserDetailMapping implements UserDetailsService {

	@Autowired
	private MongoUserRepository mongoUserRepository;

	/**
	 * 
	 * Método chamado para autenticação do usuário
	 * 
	 */
	@Override
	public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
		
		Optional<User> userOptional = mongoUserRepository.findById(id);
		UserDetails userDetails;
		
		if (userOptional.isPresent()) {
			userDetails = new UserDetailsImpl(userOptional.get());
		} else
			userDetails = null;
		
		return userDetails;
	}

}
