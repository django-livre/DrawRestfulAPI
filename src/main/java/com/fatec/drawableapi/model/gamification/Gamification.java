package com.fatec.drawableapi.model.gamification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fatec.drawableapi.service.AchievementService;

import net.sf.esfinge.gamification.mechanics.Game;
import net.sf.esfinge.gamification.proxy.GameInvoker;
import net.sf.esfinge.gamification.proxy.GameProxy;
import net.sf.esfinge.gamification.user.UserStorage;

/**
 * 
 * Operações de gamification
 * 
 */

@Component
public class Gamification {

	private Game game;
	private String user;

	@Autowired
	private Gamification(AchievementService mongo) {
		game = mongo;
		GameInvoker.getInstance().setGame(game);
	}

	public String getUser() {
		return user;
	}

	public boolean setUser(String user) {
		this.user = user;
		UserStorage.setUserID(user);
		return true;
	}

	public <E> E assertUser(E trigger) {
		return (E) GameProxy.createProxy(trigger);
	}

}
