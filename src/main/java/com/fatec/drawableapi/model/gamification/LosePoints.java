package com.fatec.drawableapi.model.gamification;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.fatec.drawableapi.api.Points;

public class LosePoints implements Points.RemovePoint {

	@Override
	public void tenPointsToUser() {
		Logger.getLogger(this.getClass().getName()).log(Level.INFO, "10 Points removed");
	}

	@Override
	public void fivePointsToUser() {
		Logger.getLogger(this.getClass().getName()).log(Level.INFO, "5 Points removed");
	}

}
