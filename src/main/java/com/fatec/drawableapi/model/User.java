package com.fatec.drawableapi.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fatec.drawableapi.api.MongoSerialize;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;

/**
 * 
 * Classe modelo do usuário
 * 
 */

@Document(collection = "users")
public class User implements MongoSerialize {

	@Id
	private String id;
	private String name;
	private String email;
	private String password;
	private String achivementType;
	private int points;

	public User() {
	};

	public User(String id, String name, String email, String password) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.achivementType = "Points";
		this.points = 0;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public String getAchivementType() {
		return achivementType;
	}

	/**
	 * Transforma o objeto em um documento do MongoDB
	 * 
	 * @return
	 */
	@Override
	public DBObject toMongoDocument() {
		BasicDBObjectBuilder builder = BasicDBObjectBuilder.start("_id", id).add("name", name).add("email", email)
				.add("password", password).add("points", points);
		return builder.get();
	}

}
