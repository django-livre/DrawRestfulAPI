package com.fatec.drawableapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 
 * Classe principal
 * 
 */
@SpringBootApplication
@ComponentScan
public class DrawableNnapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrawableNnapiApplication.class, args);
	}
}
