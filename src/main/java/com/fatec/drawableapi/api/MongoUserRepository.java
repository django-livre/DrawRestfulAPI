package com.fatec.drawableapi.api;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.fatec.drawableapi.model.User;

/**
 * 
 * Repositório para realizar processos nos dados referentes ao usuário
 * A interface MongoRepository recebe dois parâmetros por padrão
 * <T,ID> sendo, respectivamente o tipo da classe a ser mapeada e o tipo da classe da PK
 * 
 * A anotação query define consultas personalizadas
 */

public interface MongoUserRepository extends MongoRepository<User, String> {

	User findByName(String name);

	@Query(value = "{_id : ?0}", fields = "{'password':0}")
	Optional<User> findByIdWithoutPassword(String id);

	@Query(value = "{_id : ?0}", fields = "{'isLogged':0}")
	Optional<User> findUserAndAchievementById(String id);

}
