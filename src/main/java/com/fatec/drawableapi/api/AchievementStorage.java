package com.fatec.drawableapi.api;

import java.util.List;

import com.fatec.drawableapi.model.User;

import net.sf.esfinge.gamification.mechanics.database.Storage;

/**
 * 
 * Especialização da interface Storage existênte no framework esfinge gamification
 * para atender as necessidades da API 
 */
public interface AchievementStorage extends Storage {

	List<User> makeRanking();
}
