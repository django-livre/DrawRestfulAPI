package com.fatec.drawableapi.api;

import com.mongodb.DBObject;

/**
 * 
 * Interface para definição de método de serialização de objetos
 * 
 */

public interface MongoSerialize {
	DBObject toMongoDocument();
}
