package com.fatec.drawableapi.api;

import net.sf.esfinge.gamification.annotation.PointsToUser;
import net.sf.esfinge.gamification.annotation.RemovePoints;

/**
 * 
 * Definição da regra de negócio, como a gamificação da aplicação funcionará
 */

public interface Points {

	interface AddPoint {
		@PointsToUser(name = "Points", quantity = 10)
		void tenPointsToUser();

		@PointsToUser(name = "Points", quantity = 5)
		void fivePointsToUser();

	}

	interface RemovePoint {
		@RemovePoints(name = "Points", quantity = 10)
		void tenPointsToUser();

		@RemovePoints(name = "Points", quantity = 5)
		void fivePointsToUser();

	}
}
