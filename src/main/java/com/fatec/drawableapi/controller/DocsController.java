package com.fatec.drawableapi.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/docs")
public class DocsController {

	@GetMapping
	public String forwardDocs(HttpServletResponse response) {
		
		response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
		
		return "redirect:/swagger-ui.html";
	}
}
