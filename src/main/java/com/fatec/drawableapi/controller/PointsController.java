package com.fatec.drawableapi.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fatec.drawableapi.api.Points;
import com.fatec.drawableapi.model.gamification.Gamification;
import com.fatec.drawableapi.model.gamification.LosePoints;
import com.fatec.drawableapi.model.gamification.WonPoints;
import com.fatec.drawableapi.service.AchievementService;
import com.google.gson.Gson;

import net.sf.esfinge.gamification.achievement.Achievement;

@RestController
@RequestMapping("/point")
public class PointsController {

	@Autowired
	private Gamification gamification;
	@Autowired
	private AchievementService mongoDBGame;

	/***
	 * 
	 * Adiciona 10 ou 5 pontos para o usuário
	 * 
	 * @param id
	 * @param quantity
	 * @return
	 */
	
	@PostMapping(produces = "application/json")
	public ResponseEntity<String> addPoints(@RequestParam String id, @RequestParam Integer quantity) {
		
		Points.AddPoint add = new WonPoints();
		gamification.setUser(id);

		if (quantity == null) {
			return ResponseEntity.badRequest()
					.body("{\"success\": \"false\",\"description\": \"Quantity cannot be null\"}");
		} else {
			if (quantity == 10) {
				gamification.assertUser(add).tenPointsToUser();
				return ResponseEntity.ok("{\"success\": \"true\",\"description\": \"10 points add\"}");
			} else if (quantity == 5) {
				gamification.assertUser(add).fivePointsToUser();
				return ResponseEntity.ok("{\"success\": \"true\",\"description\": \"5 points add\"}");
			} else
				return ResponseEntity.badRequest()
						.body("{\"success\": \"false\",\"description\": \"Invalid quantity\"}");
		}
	}
	
	/**
	 * 
	 * Remove 10 ou 5 pontos para o usuário
	 * 
	 * @param id
	 * @param quantity
	 * @return
	 */

	@DeleteMapping(produces = "application/json")
	public ResponseEntity<String> removePoints(@RequestParam String id, @RequestParam Integer quantity) {

		Points.RemovePoint remove = new LosePoints();
		gamification.setUser(id);

		if (quantity == null) {
			return ResponseEntity.badRequest()
					.body("{\"success\": \"false\",\"description\": \"Quantity cannot be null\"}");
		} else {
			if (quantity == 10) {
				gamification.assertUser(remove).tenPointsToUser();
				return ResponseEntity.ok("{\"success\": \"true\",\"description\": \"10 points removed\"}");
			} else if (quantity == 5) {
				gamification.assertUser(remove).fivePointsToUser();
				return ResponseEntity.ok("{\"success\": \"true\",\"description\": \"5 points removed\"}");
			} else
				return ResponseEntity.badRequest()
						.body("{\"success\": \"false\",\"description\": \"Invalid quantity\"}");
		}
	}

	/**
	 * 
	 * Recupera pontos do usuário
	 * 
	 * @param id
	 * @param achievement
	 * @return
	 */
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<String> recoveryPointsById(@RequestParam String id,
			@RequestParam(required = false) String achievement) {

		Map<String, Achievement> response = new HashMap<>();

		if (achievement == null) {
			response = mongoDBGame.getAchievements(id);
		} else
			response.put(id, mongoDBGame.getAchievement(id, achievement));
		if (response.isEmpty()) {
			return ResponseEntity.badRequest()
					.body("{\"success\": \"false\",\"description\": \"User or achievement invalid\"}");
		}
		
		return ResponseEntity.ok(new Gson().toJson(response));
	}
}