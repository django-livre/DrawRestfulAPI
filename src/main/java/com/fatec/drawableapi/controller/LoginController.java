package com.fatec.drawableapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fatec.drawableapi.service.UserService;

@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	private UserService userService;

	@PostMapping
	public ResponseEntity<String> login(@RequestParam String id, @RequestParam String password) {

		boolean isUserAuthenticated = userService.loginUser(id, password);

		if (isUserAuthenticated) {
			return ResponseEntity.ok("{\"success\": true, \"description\": \"User and password matches\"}");
		}
		return ResponseEntity.status(HttpStatus.FORBIDDEN)
				.body("{\"success\": false, \"description\": \"User or password are not valid\"}");
	}
}
