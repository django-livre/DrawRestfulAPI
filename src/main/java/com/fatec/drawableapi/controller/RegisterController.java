package com.fatec.drawableapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fatec.drawableapi.model.User;
import com.fatec.drawableapi.service.UserService;

@RestController
@RequestMapping("/register")
public class RegisterController {

	@Autowired
	private UserService mongo;

	/**
	 * 
	 * Cadastra novo usuário
	 * 
	 * @param id
	 * @param name
	 * @param email
	 * @param password
	 * @return
	 */
	
	@PutMapping(produces = "application/json")
	public ResponseEntity<String> registerUser(@RequestParam("id") String id, @RequestParam("name") String name,
			@RequestParam("email") String email, @RequestParam("password") String password) {

		String encodePassword = new BCryptPasswordEncoder().encode(password);
		if (mongo.registerUser(new User(id, name, email, encodePassword))) {
			return ResponseEntity.ok("{\"success\": true, \"description\": \"Successful registration\"}");
		} else {
			return ResponseEntity.badRequest()
					.body("{\"success\": false, \"description\": \"User already registered\"}");
		}
	}
}
