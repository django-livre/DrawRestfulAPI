package com.fatec.drawableapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fatec.drawableapi.service.AchievementService;
import com.google.gson.JsonObject;

@RestController
@RequestMapping("/ranking")
public class RankingController {

	@Autowired
	private AchievementService mongoDBGame;

	/**
	 * 
	 * Recupera ranking realizando um sort dos pontos dos usuários
	 * 
	 * @return
	 */
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<String> getRanking() {
		JsonObject ranking = mongoDBGame.getRankingByUser();
		if(ranking != null) {
			return ResponseEntity.ok(ranking.toString());
		}else {
			return ResponseEntity.badRequest()
					.body("{\"success\": false, \"description\": \"Ranking could not be found\"}");
		}
	}
}
