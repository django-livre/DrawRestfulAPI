package com.fatec.drawableapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fatec.drawableapi.model.User;
import com.fatec.drawableapi.service.UserService;

@RestController
@RequestMapping("/profile")
public class ProfileController {
	
	@Autowired
	private UserService mongo;

	/**
	 * 
	 * Recupera perfil do usuário
	 * 
	 * @param id
	 * @return
	 */
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<String> recoveryProfile(@RequestParam String id) {
		User res = mongo.profileUser(id);
		if (res != null) {
			return ResponseEntity.ok(res.toMongoDocument().toString());
		}
		return ResponseEntity.badRequest().body("{\"success\": false, \"description\": \"User not found\"}");
	}
}
