package com.fatec.drawableapi.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fatec.drawableapi.api.AchievementStorage;
import com.fatec.drawableapi.model.User;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import net.sf.esfinge.gamification.achievement.Achievement;
import net.sf.esfinge.gamification.mechanics.Game;

/**
 * 
 * Operações de tratamento de dados referentes a gamification
 * 
 */

@Component
public class AchievementService extends Game {

	private AchievementStorage storage;

	@Autowired
	private AchievementService(AchievementStorage achievementStorage) {
		this.storage = achievementStorage;
	}

	@Override
	public void insertAchievement(Object user, Achievement a) {
		try {
			storage.update(user, a);
		} catch (SQLException e) {
			Logger.getLogger(this.getClass().getName(), "Metodo insert: " + e.getMessage());
		}
	}

	@Override
	public void deleteAchievement(Object user, Achievement a) {
		try {
			storage.update(user, a);
		} catch (SQLException e) {
			Logger.getLogger(this.getClass().getName(), "Metodo delete: " + e.getMessage());
		}
	}

	@Override
	public void updateAchievement(Object user, Achievement a) {
		try {
			storage.update(user, a);
		} catch (SQLException e) {
			Logger.getLogger(this.getClass().getName(), "Metodo updateAchievement: " + e.getMessage());
		}
	}

	@Override
	public Achievement getAchievement(Object user, String achievName) {
		Achievement a = null;
		try {
			a = storage.select(user, achievName);
		} catch (SQLException e) {
			Logger.getLogger(this.getClass().getName(), "Metodo getAchievement: " + e.getMessage());
		}
		return a;
	}

	@Override
	public Map<String, Achievement> getAchievements(Object user) {
		Map<String, Achievement> map = new HashMap<>();
		try {
			map = storage.select(user);
		} catch (SQLException e) {
			Logger.getLogger(this.getClass().getName(), "Metodo getAchievements: " + e.getMessage());
		}
		return map;
	}

	public JsonObject getRankingByUser() {
		JsonObject ranking = null, jsonUsers = null;
		List<User> users = storage.makeRanking();

		if (users != null) {
			ranking = new JsonObject();
			jsonUsers = new JsonObject();

			Integer position = 1;
			for (User u : users) {

				@SuppressWarnings("rawtypes")
				Map<String, Comparable> map = new LinkedHashMap<String, Comparable>();
				map.put("name", u.getName());
				map.put("position", position);
				map.put("points", u.getPoints());

				jsonUsers.add(position.toString(), new Gson().toJsonTree(map));
				position++;
			}
			ranking.add("users", jsonUsers);
		}
		return ranking;
	}
}
