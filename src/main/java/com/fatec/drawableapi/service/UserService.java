package com.fatec.drawableapi.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.fatec.drawableapi.api.MongoUserRepository;
import com.fatec.drawableapi.model.User;

/**
 * 
 * Operações de tratamento de dados referentes ao usuário
 * 
 */

@Component
public class UserService {

	@Autowired
	private MongoUserRepository mongoUserRepository;

	/**
	 * Método para registrar usuário no banco de dados
	 * 
	 * @param user
	 * @return
	 */
	public boolean registerUser(User user) {

		try {
			mongoUserRepository.insert(user);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Realiza Login de usuário já cadastrado
	 * 
	 * @param id
	 * @param password
	 * @return
	 */
	public boolean loginUser(String id, String password) {

		Optional<User> user = mongoUserRepository.findById(id);

		if (user.isPresent()) {

			User findUser = user.get();
			String passwordDB = findUser.getPassword();
			PasswordEncoder decoder = new BCryptPasswordEncoder();
			
			if (decoder.matches(password, passwordDB)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Método para recuperar o perfil do usuário
	 * 
	 * @param userID
	 * @return
	 */

	public User profileUser(String userID) {
		User user;
		Optional<User> optionalUser = mongoUserRepository.findByIdWithoutPassword(userID);
		if (optionalUser.isPresent()) {
			user = optionalUser.get();
		} else {
			user = null;
		}
		return user;

	}
}